# Card10cover

![card10cover.jpg](card10cover.jpg)

## Features

The case provides additional protection to your card10, while still giving you access to most of the sensors, buttons and LEDs.

## Editing

This repo contains an a FreeCAD file of the card10 case. You can modify it to suit your needs.

To export a .stl by selecting the bodies you want to combine and use >File>Export. I.e. to create the supplied card10case.stl you will need to select the following files:

* card10cover group

which contains:
 
  * hull

  * button_A

  * button_B

  * button_C

  * button_D

  * clip_A

  * clip_B

  * clip_C

  * clip_D

## Printing

When using an FDM printer, printing the model up side down should work best. Support needs to be activated for the large gap in the lower right corner. Some slicers may try to add support to the buttons on the left and right side. This is not necessary and can be blocked if possible.

Printing with \~0.175mm layer height or smaller with a 0.4mm nozzle should produce acceptable results. Please share your experiences with different printers and materials so others can benefit.

## Installation

Make sure you have removed all print artefacts and that the buttons can move. If necessary use a cutter to make them mobile.

To attach the case to your card10, first insert the right edge (with the two buttons close to each other) of your card10 into case and the carefully pivot the card10 into the case. Make sure to not damage the buttons while doing so. The case snaps on to the card10 and should hold onto it by itself. Additionally you can use some string to secure it to the sewing holes of the fundamental (lower) PCB.
